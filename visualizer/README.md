# vins-usb-camera-imu Visualizer

## vins-usb-camera-imu Visualizer

This visulaier tool reads data from the 2 data pipes and displays the data.  The camera data is displayed in a window.  The IMU data is printed to the terminal console.
<br />
The 2 pipes that this application publishes data on are:
* /tmp/vins_imu/ (for the IMU data).
* /tmp/vins_camera/ (for the camera data).


#### Dependencies
The required dependencies are:
* [OpenCV 4](https://docs.opencv.org/master/d7/d9f/tutorial_linux_install.html)

#### To Build
To build: ```./build.sh```

#### To Run
To run: ```cd build && ./vins-camera-imu-visualizer```
<br />
Note: "vins-camera-imu-server" must be running as well.