/**
 *  @file main.cpp
 *  @brief Main file for running the application
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Modal AI includes
#include <modal_pipe_client.h>

// 3rd Party Includes
#include "opencv2/opencv.hpp"

// C/C++ includes
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <iostream>


// Camera Constants
static constexpr uint16_t CAMERA_WIDTH_PIXELS = 640;
static constexpr uint16_t CAMERA_HEIGHT_PIXELS = 480;

/** The IMU Data
 */
struct ImuData
{
	/** The accelerometer data.
	 *  [0] -> X axis
	 *  [1] -> Y axis
	 *  [2] -> Z axis
	 */
	double accel_values[3];

	/** The gyro data.
	 *  [0] -> X axis
	 *  [1] -> Y axis
	 *  [2] -> Z axis
	 */
	double gyro_values[3];

	/** The timestamp the samples were taken at
	 */
	uint64_t timestamp;
} __attribute__((packed));

/** The Camera Data
 */
struct CameraData
{
	/** The raw pixels from the camera.
	 */
	uint8_t camera_pixels[CAMERA_WIDTH_PIXELS * CAMERA_HEIGHT_PIXELS];

	/** The timestamp the camera frame were taken at.
	 */
	uint64_t timestamp;
} __attribute__((packed));


// Client Name
const std::string CLIENT_NAME =  "vins-camera-imu-visualizer";

// The used by the vins-camera-imu-server for data  pipes
const std::string IMU_CHANNEL_DIR =  	"/tmp/vins_imu/";
const std::string CAMERA_CHANNEL_DIR =  "/tmp/vins_camera/";

// The index to use for the IMU and camera channels
static constexpr int IMU_CHANNEL_INDEX  	= 0;
static constexpr int CAMERA_CHANNEL_INDEX  	= 1;

// The buffer size of the IMU
static constexpr int IMU_PIPE_READ_BUF_SIZE = sizeof(ImuData) * 2;

// The buffer size for the camera
static constexpr int CAMERA_PIPE_READ_BUF_SIZE = sizeof(CameraData) * 2;

// If We should keep running
static bool keep_running = 1;

/** Signal Handler.  Kills the application gracefully.
 *
 *  @param[in] signal_value The signal that was issued, ignored
 */
void intHandler(int signal_value)
{
	std::cout << "Client received sigint" << std::endl;

	// Signal that we should stop running
	keep_running = false;
}

/** Data callback for the IMU.  This prints the IMU data to the terminal
 *
 *  @param[in] ch The channel index issuing the callback
 *  @param[in] data A pointer to the data
 *  @param[in] bytes The number of bytes in the data
 */
static void imu_data_callback(int ch, char* data, int bytes)
{
	// If the data is not the correct size then error out
	if(bytes != sizeof(ImuData))
	{
		std::cerr << "Got IMU data callback but had incorrect number of bytes" << std::endl;
		keep_running = 0;
		return;
	}

	// Copy the data into the IMU data buffer
	ImuData imu_data;
	std::memcpy(&imu_data, data, sizeof(imu_data));

	// Print the data
	std::cout << "IMU: ";
	std::cout << "(" << imu_data.accel_values[0] << ", ";
	std::cout << imu_data.accel_values[1] << ", ";
	std::cout << imu_data.accel_values[2] << ") (";
	std::cout << imu_data.gyro_values[0] << ", ";
	std::cout << imu_data.gyro_values[1] << ", ";
	std::cout << imu_data.gyro_values[2] << ") ";
	std::cout << imu_data.timestamp << std::endl;
}

/** Data callback for the Camera.  This displays the camera data
 *
 *  @param[in] ch The channel index issuing the callback
 *  @param[in] data A pointer to the data
 *  @param[in] bytes The number of bytes in the data
 */
static void camera_data_callback(int ch, char* data, int bytes)
{
	// If the data is not the correct size then error out
	if(bytes != sizeof(CameraData))
	{
		std::cerr << "Got Camera data callback but had incorrect number of bytes" << std::endl;
		keep_running = 0;
		return;
	}

	// Copy the data into the IMU data buffer
	CameraData camera_data;
	std::memcpy(&camera_data, data, sizeof(camera_data));

	// Convert to an opencv object
	cv::Mat image = cv::Mat(CAMERA_HEIGHT_PIXELS, CAMERA_WIDTH_PIXELS, CV_8U, camera_data.camera_pixels).clone();

	// Show the image
    cv::imshow("Camera Image Frame", image);
    if (cv::waitKey(10) == 27)
    {
        // If we pressed the escape key then exit the application
        keep_running = false;
    }

	return;
}

/** Main function
 *
 * @param[in] argc Unused
 * @param[in] argv Unused
 */
int main(int argc, char* argv[])
{
	// Register the signal so we can end the application
	signal(SIGINT, intHandler);

	// request a new pipe from the server and assign callback
	if (pipe_client_init_channel(IMU_CHANNEL_INDEX,
	                             const_cast<char*>(IMU_CHANNEL_DIR.c_str()),
	                             const_cast<char*>(CLIENT_NAME.c_str()),
	                             1, 
	                             IMU_PIPE_READ_BUF_SIZE))
	{
		std::cerr << "Client could not init IMU channel" << std::endl;
		return -1;
	}
	pipe_client_set_data_cb(IMU_CHANNEL_INDEX, imu_data_callback);


	// request a new pipe from the server and assign callback
	if (pipe_client_init_channel(CAMERA_CHANNEL_INDEX,
	                             const_cast<char*>(CAMERA_CHANNEL_DIR.c_str()),
	                             const_cast<char*>(CLIENT_NAME.c_str()),
	                             1, 
	                             CAMERA_PIPE_READ_BUF_SIZE))
	{
		std::cerr << "Client could not init IMU channel" << std::endl;
		return -1;
	}
	pipe_client_set_data_cb(CAMERA_CHANNEL_INDEX, camera_data_callback);


	// keep going until signal handler sets the running flag to 0
	while (keep_running)
	{
		usleep(100 * 1000);
	}

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	std::cout << "Client exited cleanly" << std::endl;

	return 0;
}