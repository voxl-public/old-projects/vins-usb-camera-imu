# vins-usb-camera-imu


Note: This repo assumes that all building will be done on a Linux system.

## vins-usb-camera-imu

This application will read data from the Modal AI M0021 board and publish the data to 2 pipes using libmodal_pipe.
<br />
The 2 pipes that this application publishes data on are (for desktop):
* /tmp/vins_imu/ (for the IMU data)
* /tmp/vins_camera/ (for the camera data)

<br />

For android the pipes are:
* /data/local/tmp/vins_imu/ (for the IMU data)
* /data/local/tmp/vins_camera/ (for the camera data)


## Cloning
After cloning be sure to run ```git submodule update --init --recursive``` to get the submodules needed for this application

## Running on Desktop

#### Dependencies
The required dependencies are:
* libusb-1.0

#### To Build 
To build: ```./build.sh```

#### To Run
To run: ```cd build && ./vins-camera-imu-server```

#### Visualizing the data for debugging

This repo contain a tool that reads the data from the pipes and visualizes the camera frames. It also shows the IMU data on the terminal output.  
<br />
This tool is located in the "visualizer" subdirectory and contains a README file with more information. 

## Running on Android

#### Prerequisites
In order to run this application on an Android device, the device must have root access.  Further you may have to execute ```setenforce 0``` on the Android device depending on the specific Android build.  This will set selinux to "Permissive" mode and will allow USB access if selinux is blocking access.
<br />

If the Android device only has 1 USB port then using ADB over tcpip is required.  This can be done using the following steps:
1. Get the IP address of the device using ```adb shell ifconfig``` 
2. ```adb tcpip 5555```
3. ```adb connect <ip_address_of_device> 5555```
4. Disconnect the device from the USB. It should now be possible to ```adb shell``` into the device without the device being plugged into the computer.


#### Dependencies
The following depenencies are required:
* [Android NDK Toolchain](https://developer.android.com/ndk/downloads)
* A rooted Android device. A non-rooted Android device will not allow direct access to plugged in USB devices.  An Android device with root access (su access) is required.

#### To Build
To build:
1. Locate the Android NDK toolchain, specifically the "ndk-bundle" directory.  This is usually installed in "~/Android/Sdk/ndk-bundle".
2. Build using the build_android.sh script as follows: ```./build_android.sh --ndk <location_of_ndk_from_step_1> --build_dir <build_dir>```

Notes:
* If --ndk is not specified then a default NDK location of "\~/Android/Sdk/ndk-bundle" is assumed
* If --build_dir is not specified then a default build location of "./android_build" is assumed

#### To Run
Running needs to be done from within the "/data/local/tmp/"" directory on an Android device.  Applications cannot be run from "/sdcard/".<br />

To run:
1. Push the "android_build" directory (or whatever the specified build directory is) to "/data/local/tmp/" on the Android device.
2. ```adb shell```
3. ```su```
4. ```cd /data/local/tmp/android_build/bin```
5. ```chmod 777 vins-camera-imu-server```
6. ```./vins-camera-imu-server```

#### Enabling Printing on Android
By default all standard output is piped to /dev/null.  The output can be redirected to logcat using:
1. ```adb shell stop```
2. ```adb shell setprop log.redirect-stdio true```
3. ```adb shell start```
