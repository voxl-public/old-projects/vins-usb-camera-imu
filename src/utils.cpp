/**
 *  @file utils.cpp
 *  @brief File containing utils needed by the main application
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Modal AI Includes
#include "utils.h"

// C/C++ Includes
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>	// for system()
#include <unistd.h>	// for access()
#include <errno.h>
#include <sys/stat.h>	// for mkdir and chmod
#include <sys/types.h>	// for mkdir and chmod
#include <dirent.h>	// for opendir
#include <string.h> // for memset

// local function declarations
static void shutdown_signal_handler(int signum);
static void segfault_handler(int signum, siginfo_t *info, void *context);

namespace Utils
{
// this is an extern variable!!
int main_running = 0; // main() while loop should check this for shutdown
char* pid_file;
};

int Utils::make_pid_file(const char* path)
{
	FILE *fd;
	pid_t current_pid;

	// start by checking if a pid file exists
	if (access(path, F_OK ) == 0)
	{
		fprintf(stderr, "ERROR: in make_pid_file, file already exists, a new one was not written\n");
		fprintf(stderr, "You have either called this function twice, or you need to \n");
		fprintf(stderr, "call kill_existing_process BEFORE make_pid_file\n");
		return 1;
	}

	// open new file for writing
	fd = fopen(path, "w");
	if (fd == NULL)
	{
		perror("ERROR in make_pid_file");
		return -1;
	}
	current_pid = getpid();
	fprintf(fd, "%d", (int)current_pid);
	fflush(fd);
	fclose(fd);

	pid_file = static_cast<char *>(malloc(strlen(path) + 1));
	strcpy(pid_file, path);
	return 0;
}


int Utils::kill_existing_process(const char* path, float timeout_s)
{
	FILE* fd;
	int old_pid, i, ret, num_checks;

	// sanity checks
	if (timeout_s < 0.1f)
	{
		fprintf(stderr, "ERROR in kill_existing_process, timeout_s must be >= 0.1f\n");
		return -4;
	}

	// start by checking if a pid file exists
	if (access(path, F_OK))
	{
		// PID file missing, nothing is running
		return 0;
	}
	if (access(path, W_OK))
	{
		fprintf(stderr, "ERROR, in kill_existing_process, don't have write access \n");
		fprintf(stderr, "to PID file. Existing process is probably running as root.\n");
		fprintf(stderr, "Try running 'sudo kill'\n");
		return -3;
	}
	// attempt to open PID file if it fails something very wrong with it
	fd = fopen(path, "r");
	if (fd == NULL)
	{
		fprintf(stderr, "WARNING, in kill_existing_process, PID file exists but is not\n");
		fprintf(stderr, "readable. Attempting to delete it.\n");
		remove(path);
		return -2;
	}
	// try to read the current process ID
	ret = fscanf(fd, "%d", &old_pid);
	fclose(fd);
	if (ret != 1)
	{
		// invalid contents, just delete pid file
		fprintf(stderr, "WARNING, in kill_existing_process, PID file exists but contains\n");
		fprintf(stderr, "invalid contents. Attempting to delete it.\n");
		remove(path);
		return -2;
	}

	// if the file didn't contain a PID number, remove it and
	// return -2 indicating weird behavior
	if (old_pid == 0)
	{
		fprintf(stderr, "WARNING, in kill_existing_process, PID file exists but contains\n");
		fprintf(stderr, "invalid contents. Attempting to delete it.\n");
		remove(path);
		return -2;
	}

	// check if it's our own pid, if so return 0
	if (old_pid == (int)getpid()) return 0;

	// check if PID found in file is actually still running
	if (kill((pid_t)old_pid, 0))
	{
		fprintf(stderr, "PID file exists but its process has stopped, removing file.\n");
		remove(path);
		return -2;
	}


	fprintf(stderr, "WARNING, exising instance of voxl-vision-px4 found, attempting to stop it\n");

	// process must be running, attempt a clean shutdown
	if (kill((pid_t)old_pid, SIGINT) == -1)
	{
		if (errno == EPERM)
		{
			fprintf(stderr, "ERROR in kill_existing_process, insufficient permissions to stop\n");
			fprintf(stderr, "an existing process which is probably running as root.\n");
			fprintf(stderr, "Try running 'sudo kill' to stop it.\n\n");
			return -3;
		}
		remove(path);
		return -2;
	}

	// check every 0.1 seconds to see if it closed
	num_checks = timeout_s / 0.1f;
	for (i = 0; i <= num_checks; i++)
	{
		// check if PID has stopped
		if (getpgid(old_pid) == -1)
		{
			// succcess, it shut down properly
			remove(path);
			return 1;
		}
		else
		{
			usleep(100000);
		}
	}

	// otherwise force kill the program if the PID file never got cleaned up
	kill((pid_t)old_pid, SIGKILL);
	for (i = 0; i <= num_checks; i++)
	{
		// check if PID has stopped
		if (getpgid(old_pid) == -1)
		{
			// succcess, it shut down properly
			remove(path);
			return 1;
		}
		else
		{
			usleep(100000);
		}
	}

	// delete the old PID file if it was left over
	remove(path);
	// return -1 indicating the program had to be killed
	fprintf(stderr, "WARNING in kill_existing_process, process failed to\n");
	fprintf(stderr, "close cleanly and had to be killed.\n");
	return -1;
}


int Utils::remove_pid_file(const char* path)
{
	// if PID file exists, remove it
	if (access(path, F_OK ) == 0) return remove(path);
	return 0;
}


int Utils::enable_signal_handler(void)
{
	// make the sigaction struct for shutdown signals
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = shutdown_signal_handler;

	// set actions
	if (sigaction(SIGINT, &action, NULL) < 0)
	{
		fprintf(stderr, "ERROR: failed to set sigaction\n");
		return -1;
	}
	signal(SIGINT, shutdown_signal_handler);
	if (sigaction(SIGTERM, &action, NULL) < 0)
	{
		fprintf(stderr, "ERROR: failed to set sigaction\n");
		return -1;
	}
	if (sigaction(SIGHUP, &action, NULL) < 0)
	{
		fprintf(stderr, "ERROR: failed to set sigaction\n");
		return -1;
	}
	// different handler for segfaults
	// here we want SIGINFO too so we use sigaction intead of handler
	// also use RESETHAND to stop infinite loops (doesn't work on all platforms)
	action.sa_handler = NULL;
	action.sa_sigaction = segfault_handler;
	action.sa_flags = SA_SIGINFO | SA_RESETHAND;
	if (sigaction(SIGSEGV, &action, NULL) < 0)
	{
		fprintf(stderr, "ERROR: failed to set sigaction\n");
		return -1;
	}
	return 0;
}


static void segfault_handler(__attribute__ ((unused)) int signum, __attribute__ ((unused)) siginfo_t *info, __attribute__ ((unused)) void *context)
{
	fprintf(stderr, "Fault address: %p\n", info->si_addr);
	switch (info->si_code)
	{
	case SEGV_MAPERR:
		fprintf(stderr, "Address not mapped.\n");
		break;
	case SEGV_ACCERR:
		fprintf(stderr, "Access to this address is not allowed.\n");
		break;
	default:
		fprintf(stderr, "Unknown reason.\n");
		break;
	}
	Utils::main_running = 0;


	// reset the signal handler to prevent infinite loop,
	// this shouldn't be necessary with the SA_RESETHAND flag but is on some platforms
	struct sigaction action;
	action.sa_sigaction = NULL;
	action.sa_handler = NULL;
	action.sa_flags = SA_SIGINFO | SA_RESETHAND;
	if (sigaction(SIGSEGV, &action, NULL) < 0)
	{
		fprintf(stderr, "ERROR: failed to set sigaction\n");
		return;
	}

	if (Utils::pid_file)
	{
		Utils::remove_pid_file(Utils::pid_file);
	}

	return;
}


static void shutdown_signal_handler(int signo)
{
	switch (signo)
	{
	case SIGINT: // normal ctrl-c shutdown interrupt
		fprintf(stderr, "\nreceived SIGINT Ctrl-C\n");
		Utils::main_running = 0;
		break;
	case SIGTERM: // catchable terminate signal
		fprintf(stderr, "\nreceived SIGTERM\n");
		Utils::main_running = 0;
		break;
	case SIGHUP: // terminal closed or disconnected, carry on anyway
		break;
	default:
		break;
	}
	return;
}