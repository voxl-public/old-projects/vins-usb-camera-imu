/**
 *  @file VinsCameraImu.cpp
 *  @brief Driver Class for the Modal AI M0018 VINS Camera-IMU Board
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Modal AI Includes
#include "VinsCameraImu.h"

// C++ includes
#include <string>
#include <functional>
#include <unistd.h>
#include <iomanip>
#include <cstring>
#ifndef __ANDROID__
#include <stdexcept>
#endif

VinsCameraImu::VinsCameraImu()
{
	// Init the camera
	this->initCamera();

	// Init the IMU
	this->initImu();
}

VinsCameraImu::~VinsCameraImu()
{
	// Stop the camera stream if it is running
	if (this->camera_is_streaming)
	{
		this->stopCameraStream();
	}

	// Close the device
	if (this->camera_device_handle)
	{
		uvc_close(this->camera_device_handle);
		this->camera_device_handle = nullptr;
	}

	// Release the device descriptor
	if (this->camera_device)
	{
		uvc_unref_device(this->camera_device);
		this->camera_device = nullptr;
	}

	// Close the context since we are exiting
	if (this->camera_uvc_context)
	{
		uvc_exit(this->camera_uvc_context);
		this->camera_uvc_context = nullptr;
	}

	// stop the IMU if it is running
	this->stopImuDataStream();

	// Cleanup the IMU
	if (this->imu_initted)
	{
		libusb_free_transfer(this->imu_device_transfer);
		libusb_close(this->imu_device_handle);
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);
	}
}

void VinsCameraImu::initCamera()
{
	uvc_error_t result;

	/* Initialize a UVC service context. Libuvc will set up its own libusb
	 * context. Replace NULL with a libusb_context pointer to run libuvc
	 * from an existing libusb context. */
	result = uvc_init(&(this->camera_uvc_context), NULL);
	if (result < 0)
	{
		std::string error_msg = "Had error initing UVC: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	// Locates the first attached UVC device, stores in dev
	result = uvc_find_device(
	             this->camera_uvc_context,
	             &(this->camera_device),
	             BOARD_VENDOR_ID,
	             BOARD_PRODUCT_ID,
	             NULL);
	if (result < 0)
	{
		// Close the context since we are exiting
		uvc_exit(this->camera_uvc_context);
		this->camera_uvc_context = nullptr;

		// Throw the error
		std::string error_msg = "Could not find device with correct USB Vendor/Product ID: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	// Open the Camera device
	result = uvc_open(this->camera_device, &(this->camera_device_handle));
	if (result < 0)
	{
		// Release the device descriptor
		uvc_unref_device(this->camera_device);
		this->camera_device = nullptr;

		// Close the context since we are exiting
		uvc_exit(this->camera_uvc_context);
		this->camera_uvc_context = nullptr;

		// Throw the error
		std::string error_msg = "Could not open camera device: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	/** Get a negotiated camera control parameter set.
	 *  This will not set the camera controls but just negotiate
	 *  to get what the device describes it has
	 */
	result = uvc_get_stream_ctrl_format_size(
	             this->camera_device_handle,
	             &(this->camera_control),
	             UVC_FRAME_FORMAT_GRAY8,
	             CAMERA_WIDTH_PIXELS,
	             CAMERA_HEIGHT_PIXELS,
	             CAMERA_FPS);
	if (result < 0)
	{
		// Close the device
		uvc_close(this->camera_device_handle);
		this->camera_device_handle = nullptr;

		// Release the device descriptor
		uvc_unref_device(this->camera_device);
		this->camera_device = nullptr;

		// Close the context since we are exiting
		uvc_exit(this->camera_uvc_context);
		this->camera_uvc_context = nullptr;

		// Throw the error
		std::string error_msg = "Could not get camera control parameters: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}
}

void VinsCameraImu::initImu()
{
	int ret_code = 0;

	// Init the libusb library
	ret_code = libusb_init(&(this->usb_context));
	if (ret_code != LIBUSB_SUCCESS)
	{
		throw std::runtime_error("Could not init libusb library.");
	}

	// Get the device list
	int num_devices = libusb_get_device_list(this->usb_context, &(this->usb_device_list));
	if (num_devices < 0)
	{
		// Close the libusb library
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("Could not find any USB devices.");
	}

	// Open the USB device
	this->imu_device_handle = libusb_open_device_with_vid_pid(this->usb_context,
	                          BOARD_VENDOR_ID,
	                          BOARD_PRODUCT_ID);
	if (!this->imu_device_handle)
	{
		// Cleanup
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("Could not open IMU USB device.");
	}

	// Make sure the device is not already active
	if (libusb_kernel_driver_active(this->imu_device_handle, IMU_DEVICE_INTERFACE))
	{
		// Cleanup
		libusb_close(this->imu_device_handle);
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("IMU device is already active.");
	}

	// Claim the IMU interface
	ret_code = libusb_claim_interface(this->imu_device_handle, IMU_DEVICE_INTERFACE);
	if (ret_code != LIBUSB_SUCCESS)
	{
		// Cleanup
		libusb_close(this->imu_device_handle);
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("Could not claim IMU interface.");
	}

	// Change the interface settings
	ret_code = libusb_set_interface_alt_setting(this->imu_device_handle, 2, 0);
	if (ret_code != LIBUSB_SUCCESS)
	{
		// Cleanup
		libusb_close(this->imu_device_handle);
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("Could not configure IMU interface.");
	}


	// Setup the IMU transfer
	this->imu_device_transfer = libusb_alloc_transfer(IMU_TRANSFER_NUMBER_OF_PACKETS);
	if (this->imu_device_transfer == nullptr)
	{
		// Cleanup
		libusb_close(this->imu_device_handle);
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("Failed to allocate IMU data");
	}

	// Fill the transfer fields for the IMU iso transfer
	libusb_fill_iso_transfer(this->imu_device_transfer,
	                         this->imu_device_handle,
	                         IMU_DEVICE_ENDPOINT,
	                         this->imu_transfer_buffer,
	                         IMU_TRANSFER_DEV_PACKET_LENGTH,
	                         IMU_TRANSFER_NUMBER_OF_PACKETS,
	                         VinsCameraImu::imuDataReadyInternalCallbackHelper,
	                         this,
	                         0);

	// Set the ISO packet lengths
	libusb_set_iso_packet_lengths(this->imu_device_transfer, IMU_TRANSFER_DEV_PACKET_LENGTH);

	// Do the USB transfers
	ret_code = libusb_submit_transfer(this->imu_device_transfer);
	if (ret_code != LIBUSB_SUCCESS)
	{
		// Cleanup
		libusb_free_transfer(this->imu_device_transfer);
		libusb_close(this->imu_device_handle);
		libusb_free_device_list(this->usb_device_list, 1);
		libusb_exit(this->usb_context);

		// throw the exception
		throw std::runtime_error("Could not configure IMU interface.");
	}

	// The IMU was initted
	this->imu_initted = true;
}

void VinsCameraImu::startCameraStream()
{
	// The camera is already streaming so do nothing
	if (this->camera_is_streaming)
	{
		return;
	}

	// Make sure the camera is enabled
	if (this->camera_device_handle == nullptr)
	{
		throw std::runtime_error("Camera Device not initialized");
	}

	// Start the stream
	uvc_error_t result = uvc_start_streaming(this->camera_device_handle, &(this->camera_control), cameraFrameReadyInternalCallbackHelper, this, 0);
	if (result < 0)
	{
		std::string error_msg = "Could not start camera stream: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	// Mark the camera as is streaming
	this->camera_is_streaming = true;

	/** When starting the stream, the camera resets its gain and exposure.
	 *  So we need to resend the gain and exposure settings to return to
	 *  what the user wants
	 */

	// Set the gain if the user set it previously
	if (this->last_set_gain != std::numeric_limits<uint16_t>::max())
	{
		this->setCameraGain(this->last_set_gain);
	}

	// Set the exposure if the user set it previously
	if (this->last_set_exposure != std::numeric_limits<uint32_t>::max())
	{
		this->setCameraExposure(this->last_set_exposure);
	}
}

void VinsCameraImu::stopCameraStream()
{
	// Nothing to do if the camera is already not streaming
	if (!this->camera_is_streaming)
	{
		return;
	}

	// Make sure the camera is enabled
	if (this->camera_device_handle == nullptr)
	{
		throw std::runtime_error("Camera Device not initialized");
	}

	// Stop the stream
	uvc_stop_streaming(this->camera_device_handle);

	// Mark the camera as not streaming
	this->camera_is_streaming = false;
}

void VinsCameraImu::setCameraExposure(uint32_t new_exposure)
{
	uint32_t new_exposure_orig = new_exposure;

	// Make sure the exposure value is valid
	if ((new_exposure < CAMERA_EXPOSURE_MIN_USEC) || (new_exposure > CAMERA_EXPOSURE_MAX_USEC))
	{
		std::string error_msg = "Invalid exposure value: ";
		error_msg += std::to_string(new_exposure);
		throw std::runtime_error(error_msg);
	}


	// Make sure the camera is enabled
	if (this->camera_device_handle == nullptr)
	{
		throw std::runtime_error("Camera Device not initialized");
	}

	// Convert the new exposure to the correct units
	new_exposure /= 100;

	// Set the exposure
	uvc_error_t result = uvc_set_exposure_abs(this->camera_device_handle, new_exposure);
	if (result < 0)
	{
		std::string error_msg = "Could not set camera exposure: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	// Cache the last set exposure
	this->last_set_exposure = new_exposure_orig;
}

uint32_t VinsCameraImu::getCameraExposure()
{
	// Make sure the camera is enabled
	if (this->camera_device_handle == nullptr)
	{
		throw std::runtime_error("Camera Device not initialized");
	}

	// Get the exposure
	uint32_t exposure = 0;
	uvc_error_t result = uvc_get_exposure_abs(this->camera_device_handle, &exposure, UVC_GET_CUR);
	if (result < 0)
	{
		std::string error_msg = "Could not get camera exposure: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	// Convert to the correct units
	exposure *= 100;

	return exposure;
}

void VinsCameraImu::setCameraGain(uint16_t new_gain)
{
	// Make sure the exposure value is valid
	if ((new_gain < CAMERA_GAIN_MIN_VALUE) || (new_gain > CAMERA_GAIN_MAX_VALUE))
	{
		std::string error_msg = "Invalid gain value: ";
		error_msg += std::to_string(new_gain);
		throw std::runtime_error(error_msg);
	}

	// Make sure the camera is enabled
	if (this->camera_device_handle == nullptr)
	{
		throw std::runtime_error("Camera Device not initialized");
	}

	// Set the gain
	uvc_error_t result = uvc_set_gain(this->camera_device_handle, new_gain);
	if (result < 0)
	{
		std::string error_msg = "Could not set camera gain: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	// Cache the last set gain
	this->last_set_gain = new_gain;
}

uint16_t VinsCameraImu::getCameraGain()
{
	// Make sure the camera is enabled
	if (this->camera_device_handle == nullptr)
	{
		throw std::runtime_error("Camera Device not initialized");
	}

	// Get the exposure
	uint16_t gain = 0;
	uvc_error_t result = uvc_get_gain(this->camera_device_handle, &gain, UVC_GET_CUR);
	if (result < 0)
	{
		std::string error_msg = "Could not get camera gain: ";
		error_msg += uvc_strerror(result);
		throw std::runtime_error(error_msg);
	}

	return gain;
}

void VinsCameraImu::startImuDataStream(uint16_t frequency)
{
	// Make sure the imu is initted
	if (!this->imu_initted)
	{
		throw std::runtime_error("IMU Device not initialized");
	}

	// Make sure the frequency is valid
	if ((frequency > IMU_MAX_FREQUENCY) || (frequency == 0))
	{
		std::string error_msg = "Invalid IMU frequency: ";
		error_msg += std::to_string(frequency);
		throw std::runtime_error(error_msg);
	}

	// The stream is already running so do nothing
	if (this->imu_thread_keep_running)
	{
		return;
	}

	// Compute the period from the frequency
	double period = frequency;
	period = 1.0 / period;

	// Convert the period from seconds to us
	period *= 1000000.0;
	this->imu_loop_period = static_cast<uint32_t>(period);

	// Set the running flags
	this->imu_thread_keep_running = true;

	// Start the thread
	this->imu_thread = std::thread(&VinsCameraImu::imuWorkerFunction, this);
}

void VinsCameraImu::stopImuDataStream()
{
	// Make sure the imu is initted
	if (!this->imu_initted)
	{
		throw std::runtime_error("IMU Device not initialized");
	}


	// The stream is not running so do nothing
	if (!this->imu_thread_keep_running)
	{
		return;
	}

	// Signal the thread to stop running
	this->imu_thread_keep_running = false;

	// Wait for the thread to stop
	if (this->imu_thread.joinable())
	{
		this->imu_thread.join();
	}
}

void VinsCameraImu::registerCameraFrameCallback(CameraFrameReadyCallback callback)
{
	// Just override any other callbacks that exist.  We only support 1 callback right now
	this->camera_frame_ready_callback = callback;
}

void VinsCameraImu::registerImuDataCallback(ImuDataReadyCallback callback)
{
	// Just override any other callbacks that exist.  We only support 1 callback right now
	this->imu_data_ready_callback = callback;
}

void VinsCameraImu::setGyroZeroOffset(const double offsets[3])
{
	/** Only allow the offset to be set before the stream is started.
	 *  we could use locks to allow the user to change the offset whenever they want
	 *  but this only happens once so just force the user to do this before the IMU is started
	 */
	if (this->imu_thread_keep_running)
	{
		throw std::runtime_error("Cant change gyro zero offset while gyro is streaming data.");
	}

	// Set the gyro zero offsets
	for (uint8_t i = 0; i < 3; i++)
	{
		this->gyro_zero_offset[i] = offsets[i];
	}
}

void VinsCameraImu::cameraFrameReadyInternalCallbackHelper(uvc_frame_t *frame, void *ptr)
{
	// Convert to the object
	VinsCameraImu *device_ptr = static_cast<VinsCameraImu *>(ptr);

	// Trampoline to the internal function
	device_ptr->cameraFrameReadyInternalCallback(frame);
}

void VinsCameraImu::cameraFrameReadyInternalCallback(uvc_frame_t *frame)
{
	CameraData data;

	// Copy the image data over
	std::memcpy(data.camera_pixels, frame->data, CAMERA_HEIGHT_PIXELS * CAMERA_WIDTH_PIXELS);

	// Extract the timestamp
	uint64_t timestamp = 0;
	std::memcpy(&timestamp, data.camera_pixels, sizeof(timestamp));

	// Convert the timestamp from microseconds to nanoseconds
	timestamp *= 1000;

	// Set the timestamp data as 0
	std::memset(data.camera_pixels, 0, sizeof(timestamp));

	// Issue the callback
	if (this->camera_frame_ready_callback)
	{
		camera_frame_ready_callback(data);
	}
}

void VinsCameraImu::imuDataReadyInternalCallbackHelper(struct libusb_transfer *transfer)
{
	// Convert to the object
	VinsCameraImu *device_ptr = static_cast<VinsCameraImu *>(transfer->user_data);

	// Trampoline to the internal function
	device_ptr->imuDataReadyInternalCallback(transfer);
}

void VinsCameraImu::imuDataReadyInternalCallback(struct libusb_transfer *transfer)
{
	// Ignore errors for the IMU
	if (transfer->status != LIBUSB_TRANSFER_COMPLETED)
	{
		return;
	}

	// Ignore data that isnt the correct packet length
	if (transfer->iso_packet_desc[0].actual_length != sizeof(ImuDataRaw))
	{
		libusb_submit_transfer(transfer);
		return;
	}

	// Extract the raw data
	ImuDataRaw imu_data_raw;
	memcpy(&imu_data_raw, transfer->buffer, transfer->iso_packet_desc[0].actual_length);

	// Convert the data
	ImuData imu_data_converted;
	this->convertImuData(imu_data_raw, imu_data_converted);

	// Issue the callback
	if (this->imu_data_ready_callback)
	{
		imu_data_ready_callback(imu_data_converted);
	}

	// Clear the transfer
	libusb_submit_transfer(transfer);
}

void VinsCameraImu::convertImuData(const ImuDataRaw &raw, ImuData &converted)
{
	// Convert the Accel data
	for (uint8_t i = 0; i < 3; i++)
	{
		converted.accel_values[i] = raw.accel_raw[i];
		converted.accel_values[i] /= IMU_ACCEL_LSB_PER_G;
	}

	// Convert the Gyro data
	for (uint8_t i = 0; i < 3; i++)
	{
		// Scale the data
		converted.gyro_values[i] = raw.gyro_raw[i];
		converted.gyro_values[i] /= IMU_GYRO_LSB_PER_DEG_PER_SEC;

		// Subtract the zero offset
		converted.gyro_values[i] -= this->gyro_zero_offset[i];
	}

	// Convert the timestamp from us to ns
	converted.timestamp = raw.timestamp_us * 1000;
}

void VinsCameraImu::imuWorkerFunction()
{
	while (this->imu_thread_keep_running)
	{
		// Handle things
		libusb_handle_events(this->usb_context);

		// Sleep for the correct period time
		usleep(this->imu_loop_period);
	}
}

