/**
 *  @file main.cpp
 *  @brief Main file for running the application
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Modal AI Includes
#include "VinsCameraImu.h"
#include "utils.h"
#include <modal_pipe_server.h>

// C++ Includes
#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


#ifdef __ANDROID__
// The PID file for this application
const std::string PID_FILE = "/data/local/tmp/vins-camera-imu-server.pid";

// this is the directory used by the vins-camera-imu-server for data  pipes
const std::string IMU_CHANNEL_DIR =  	"/data/local/tmp/vins_imu/";
const std::string CAMERA_CHANNEL_DIR =  "/data/local/tmp/vins_camera/";

#else
// The PID file for this application
const std::string PID_FILE = "/tmp/vins-camera-imu-server.pid";

// this is the directory used by the vins-camera-imu-server for data  pipes
const std::string IMU_CHANNEL_DIR =  	"/tmp/vins_imu/";
const std::string CAMERA_CHANNEL_DIR =  "/tmp/vins_camera/";

#endif



// The index to use for the IMU and camera channels
static constexpr int IMU_CHANNEL_INDEX  	= 0;
static constexpr int CAMERA_CHANNEL_INDEX  	= 1;

// The gyro values so that the gyro offset can be computed
static double gyro_values_sum[3] = {0, 0, 0};
static double number_of_gyro_samples = 0;;

/** Process new camera data that comes in
 *
 *  @param[in] data The camera data
 */
void processNewCameraFrame(VinsCameraImu::CameraData& camera_data)
{
	std::cout << "Got Camera Frame: " << camera_data.timestamp << "ns" << std::endl;

	// Send the data over the pipe
	char* data_ptr = reinterpret_cast<char*>(&camera_data);
	pipe_server_send_to_channel(CAMERA_CHANNEL_INDEX, data_ptr, sizeof(camera_data));
}

/** Process new imu data that comes in
 *
 *  @param[in] imu_data The new imu data
 */
void processNewIMUData(VinsCameraImu::ImuData &imu_data)
{
	// Get add to get the gyro offset
	gyro_values_sum[0] += imu_data.gyro_values[0];
	gyro_values_sum[1] += imu_data.gyro_values[1];
	gyro_values_sum[2] += imu_data.gyro_values[2];
	number_of_gyro_samples++;

	// Print the data
	std::cout << "IMU: ";
	std::cout << "(" << imu_data.accel_values[0] << ", ";
	std::cout << imu_data.accel_values[1] << ", ";
	std::cout << imu_data.accel_values[2] << ") (";
	std::cout << imu_data.gyro_values[0] << ", ";
	std::cout << imu_data.gyro_values[1] << ", ";
	std::cout << imu_data.gyro_values[2] << ") ";
	std::cout << imu_data.timestamp << std::endl;

	// Send the data over the pipe
	char* data_ptr = reinterpret_cast<char*>(&imu_data);
	pipe_server_send_to_channel(IMU_CHANNEL_INDEX, data_ptr, sizeof(imu_data));
}

/** Main Function
 *
 *  @param[in] argc Argument count
 *  @param[in] argv Arguments
 */
int main(int argc, char **argv)
{
	// Register the signal so we can end the application
	// signal(SIGINT, intHandler);

	/** make sure another instance isn't running
	 * if return value is -3 then a background process is running with
	 * higher privaledges and we couldn't kill it, in which case we should
	 * not continue or there may be hardware conflicts. If it returned -4
	 * then there was an invalid argument that needs to be fixed.
	 */
	if (Utils::kill_existing_process(PID_FILE.c_str(), 2.0) < -2)
	{
		return -1;
	}

	// start signal handler so we can exit cleanly
	if (Utils::enable_signal_handler() == -1)
	{
		std::cerr << "ERROR: failed to start signal handler" << std::endl;
		return -1;
	}

	// Start the IMU data channel
	if (pipe_server_init_channel(IMU_CHANNEL_INDEX, const_cast<char*>(IMU_CHANNEL_DIR.c_str()), 0))
	{
		std::cerr << "ERROR: failed to start IMU data channel" << std::endl;
		return -1;
	}

	// Start the Camera data channel
	if (pipe_server_init_channel(CAMERA_CHANNEL_INDEX, const_cast<char*>(CAMERA_CHANNEL_DIR.c_str()), 0))
	{
		std::cerr << "ERROR: failed to start Camera data channel" << std::endl;
		return -1;
	}

	/** Make PID file to indicate your project is running
	 *  due to the check made on the call to kill_existing_process() above
	 *  we can be fairly confident there is no PID file already and we can
	 *  make our own safely.
	 */
	Utils::make_pid_file(PID_FILE.c_str());

	try
	{
		// Create the VinsCameraImu object
		VinsCameraImu camera_imu_device;

		// Register the callbacks
		camera_imu_device.registerCameraFrameCallback(processNewCameraFrame);
		camera_imu_device.registerImuDataCallback(processNewIMUData);

		// Set the camera exposure to 10ms
		camera_imu_device.setCameraExposure(10000);

		// Set the camera gain to 50 (range is 1-1023)
		camera_imu_device.setCameraGain(50);

		// Start the camera streaming
		camera_imu_device.startCameraStream();

		/** Example of how to set the zero offset for the gyros
		 *  This should be done before starting the IMU
		 */
		// double gyro_zero_offset[3] = { -22.0775, 6.02569, 13.2252};
		// camera_imu_device.setGyroZeroOffset(gyro_zero_offset);

		// Start the IMU data streaming at 200 Hz
		camera_imu_device.startImuDataStream(200);

		// Loop until we say we are done
		Utils::main_running = 1;
		while (Utils::main_running)
		{
			// Sleep 100ms
			usleep(100 * 1000);
		}
	}
	catch (const std::runtime_error& error)
	{
		std::cerr << "Had runtime Error: " << error.what() << std::endl;
	}

	// Compute the gyro offset and print it if we have gyro data
	if (number_of_gyro_samples > 0)
	{
		for (uint8_t i = 0; i < 3; i++)
		{
			double offset = gyro_values_sum[i] / number_of_gyro_samples;
			std::cout << "Gyro offset " << i << ": " << offset << std::endl;
		}
	}

	// Shutdown after running
	std::cout << "Starting shutdown sequence" << std::endl;
	pipe_server_close_all_channels();
	Utils::remove_pid_file(PID_FILE.c_str());
	std::cout << "exiting cleanly" << std::endl;

	return 0;
}
