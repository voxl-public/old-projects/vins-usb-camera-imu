# !/bin/bash

set -e

# Setup the android NDK
export NDK=~/Android/Sdk/ndk-bundle/

# Where we will be installing the android build
BUILD_INSTALL_DIR=./android_build/

####################################################################################
## Parse the command line arguments
####################################################################################
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -b|--build_dir)
    BUILD_DIR_INPUT="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--ndk)
    NDK_INPUT="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    HELP_INPUT="wasSet"
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


# Check if the help was selected and if it was then print it
if [[ -v HELP_INPUT ]]; 
then 
    echo "Build vins-usb-camera-imu for Android."
    echo "This will also build all the needed dependencies."
    echo
    echo "Usage: ./build_android.sh -n <ndk_install_location> -b <output_build_dir>"
    echo "Options:"
    echo "      -n,--ndk        : The location that the NDK is installed."
    echo "                        Default is \"~/Android/Sdk/ndk-bundle/\""
    echo "                        The argument is optional"
    echo "      -b,--build_dir  : The location to put the build output"
    echo "                        Default is \"./android_build/\""
    echo "                        The argument is optional"
    echo "      -h,--help       : Display this usage help"
    exit
fi

# Check if the build dir is set and take appropriate action
if [ -z ${BUILD_DIR_INPUT+x} ]; 
then 
	# Make it absolute
	CWD=$(pwd)
	BUILD_INSTALL_DIR=${CWD}/${BUILD_INSTALL_DIR}
	echo "Build dir is not set. Using ${BUILD_INSTALL_DIR}"
else 
	echo "Build directory was manually set to ${BUILD_DIR_INPUT}"
	BUILD_INSTALL_DIR=${BUILD_DIR_INPUT}
fi

# Check if the NDK is set and take appropriate action
if [ -z ${NDK_INPUT+x} ]; 
then 
	echo "NDK is not set. Using ${NDK}"
else 
	echo "NDK was manually set to ${NDK_INPUT}"
	export NDK=${NDK_INPUT}
fi


####################################################################################
## Set the output dirs
####################################################################################

# Where the output files will go
EXEC_DIR=${BUILD_INSTALL_DIR}/bin/
LIB_DIR=${BUILD_INSTALL_DIR}/lib/
INCLUDE_DIR=${BUILD_INSTALL_DIR}/include/

# Make all the needed output directories
mkdir -p ${EXEC_DIR}
mkdir -p ${LIB_DIR}
mkdir -p ${INCLUDE_DIR}


####################################################################################
## Build Libusb
####################################################################################

# Build Libusb
CWD=$(pwd)
rm -rf ./submoduled_dependencies/libusb/android/obj
rm -rf ./submoduled_dependencies/libusb/android/libs
cd ./submoduled_dependencies/libusb/android/jni
$NDK/ndk-build -e APP_ABI=arm64-v8a -e APP_LDFLAGS="-llog -rpath ../lib"
cd ${CWD}

# Install Libusb
cp ./submoduled_dependencies/libusb/android/libs/arm64-v8a/libusb1.0.so ${LIB_DIR}
cp ./submoduled_dependencies/libusb/libusb/libusb.h ${INCLUDE_DIR}


####################################################################################
## Build the other dependencies
####################################################################################

# Build the other dependencies
depend=(
	libuvc 
	libmodal_pipe
)

CWD=$(pwd)
for i in "${depend[@]}"; do
	cd submoduled_dependencies/${i}
	./build_android.sh -b ${BUILD_INSTALL_DIR} -n ${NDK}
	cd ${CWD}
done


####################################################################################
## Build the app
####################################################################################

# The directory where we build android from
ANDROID_BUILD_DIR=./android/

# The location of the lib and include 
EXEC_BUILD_FILE=${ANDROID_BUILD_DIR}/libs/arm64-v8a/vins-usb-camera-imu-server

# Clean the Android build artifacts
rm -rf ${ANDROID_BUILD_DIR}/obj
rm -rf ${ANDROID_BUILD_DIR}/libs

# Make the android libs
CWD=$(pwd)
cd ${ANDROID_BUILD_DIR}/jni
$NDK/ndk-build -e ALL_BUILD_DIR=${BUILD_INSTALL_DIR} -e APP_ABI=arm64-v8a
cd ${CWD}

# Move the files to the Install directory 
cp -rp ${EXEC_BUILD_FILE} ${EXEC_DIR}
